import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AffenpinschersComponent } from './affenpinschers.component';

describe('AffenpinschersComponent', () => {
  let component: AffenpinschersComponent;
  let fixture: ComponentFixture<AffenpinschersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AffenpinschersComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AffenpinschersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
