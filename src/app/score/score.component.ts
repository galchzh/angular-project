import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-score',
  templateUrl: './score.component.html',
  styleUrls: ['./score.component.css']
})
export class ScoreComponent implements OnInit {

  score = '';
  type = '';
  constructor(private route: ActivatedRoute,private router:Router) { }

  ngOnInit() {
    this.route.queryParams
    .subscribe(params => {
      console.log(params); 
      this.score = params.score;
      this.type = params.type;
      console.log(this.score); 
    });
  }

  back()
  {
    this.router.navigate(['/quiz']);
  }

}
