import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.css']
})
export class MainComponent implements OnInit {

  toLogin()
  {
    this.router.navigate(['/login']);
  }

  toRegister()
  {
    this.router.navigate(['/register']);
  }

  constructor(private router:Router) { }

  ngOnInit() {
  }

}
