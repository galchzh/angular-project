import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule } from '@angular/forms';
import {ReactiveFormsModule} from '@angular/forms';

import { PickerModule } from '@ctrl/ngx-emoji-mart';


import { environment } from '../environments/environment';
import { Routes, RouterModule } from '@angular/router';

//firabase modules
import { AngularFireModule } from '@angular/fire';
import { AngularFireDatabaseModule } from '@angular/fire/database';
import { AngularFireAuthModule } from '@angular/fire/auth';

import { MatCardModule } from '@angular/material/card';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatButtonModule } from '@angular/material/button';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatGridListModule } from '@angular/material/grid-list';
import { MatIconModule } from '@angular/material/icon';
import { MatSelectModule } from '@angular/material/select';
import { MatRadioModule } from '@angular/material/radio';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatTabsModule } from '@angular/material/tabs';


import { AppComponent } from './app.component';

import { LoginComponent } from './login/login.component';
import { LoginnComponent } from './loginn/loginn.component';
import { RegisterComponent } from './register/register.component';
import { WelcomeComponent } from './welcome/welcome.component';
import { NavComponent } from './nav/nav.component';
import { ChatComponent } from './chat/chat.component';
import { CategoriesComponent } from './categories/categories.component';
import { Category1Component } from './category1/category1.component';
import { Category2Component } from './category2/category2.component';
import { Category3Component } from './category3/category3.component';
import { Category4Component } from './category4/category4.component';
import { AffenpinschersComponent } from './affenpinschers/affenpinschers.component';
import { AfghanComponent } from './afghan/afghan.component';
import { AlskenKleeComponent } from './alsken-klee/alsken-klee.component';
import { AlskenMelmoteComponent } from './alsken-melmote/alsken-melmote.component';
import { AmericanBulldogComponent } from './american-bulldog/american-bulldog.component';
import { AmericanEskimosComponent } from './american-eskimos/american-eskimos.component';
import { AmericanPitComponent } from './american-pit/american-pit.component';
import { AmstaffComponent } from './amstaff/amstaff.component';
import { JapanisAkitaComponent } from './japanis-akita/japanis-akita.component';
import { QuizComponent } from './quiz/quiz.component';
import { DogsComponent } from './dogs/dogs.component';
import { GroupRegisterComponent } from './group-register/group-register.component';
import { SuccessComponent } from './success/success.component';
import { MainComponent } from './main/main.component';
import { ScoreComponent } from './score/score.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    LoginnComponent,
    RegisterComponent,
    WelcomeComponent,
    NavComponent,
    ChatComponent,
    CategoriesComponent,
    Category1Component,
    Category2Component,
    Category3Component,
    Category4Component,
    AffenpinschersComponent,
    AfghanComponent,
    AlskenKleeComponent,
    AlskenMelmoteComponent,
    AmericanBulldogComponent,
    AmericanEskimosComponent,
    AmericanPitComponent,
    AmstaffComponent,
    JapanisAkitaComponent,
    QuizComponent,
    DogsComponent,
    GroupRegisterComponent,
    SuccessComponent,
    MainComponent,
    ScoreComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFireDatabaseModule,
    AngularFireAuthModule,
    MatCardModule,
    MatFormFieldModule,
    MatInputModule,
    MatButtonModule,
    MatIconModule,
    MatToolbarModule,
    MatTabsModule,
    MatRadioModule,
    MatSelectModule,
    MatCheckboxModule,
    MatGridListModule,
    PickerModule,
    RouterModule.forRoot([
      { path:'', component: MainComponent },
      { path:'welcome', component: WelcomeComponent },
      { path:'login', component: LoginComponent },
      { path:'loginn', component: LoginnComponent },
      { path:'register', component: RegisterComponent },
      { path:'chat', component: ChatComponent },
      {path:'categories',component: CategoriesComponent},
      {path:'category1',component: Category1Component},
      {path:'category2',component: Category2Component},
      {path:'category3',component: Category3Component},
      {path:'category4',component: Category4Component},
      {path:'alskenMelmote',component:AlskenMelmoteComponent},
      {path:'affenpinschers',component:AffenpinschersComponent},
      {path:'afghan',component:AfghanComponent}, 
      {path:'alskenKlee',component:AlskenKleeComponent},
      {path:'americanBulldog',component:AmericanBulldogComponent},
      {path:'americanEskimos',component:AmericanEskimosComponent},
      {path:'americanPit',component:AmericanPitComponent}, 
      {path:'amstaff',component:AmstaffComponent},
      {path:'japanisAkita',component:JapanisAkitaComponent},
      {path:'alskenKlee',component:AlskenKleeComponent},
      {path: 'quiz',component:QuizComponent},
      {path: 'groupRegister',component:GroupRegisterComponent},
      {path: 'success',component:SuccessComponent},
      {path: 'dogs',component:DogsComponent},
      {path: 'score',component:ScoreComponent},
      { path:'main', component: MainComponent },
      { path:'**', component: MainComponent }
    ])
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }