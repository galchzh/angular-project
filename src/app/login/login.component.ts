import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';
import { Router } from '@angular/router';
import { AngularFireAuth } from '@angular/fire/auth';
import { auth } from 'firebase/app';

@Component({
  selector: 'login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  email: string;
  password: string;
  name: string;
  error = '';
  output = '';
  required = '';
  require = true;
  flag = false;
  specialChar = ['!','@','#','$','%','^','&','*','(',')','_','-','+','='];
  valid = false;
  validation = "";
  
  login()
  {
    this.authService.login(this.email,this.password)
    .then(user =>
    {
      this.router.navigate(['/welcome'])
    }).catch(err => {
      this.error = err.code;
      this.output = err.message;
      this.flag = true;
    })
  }


  onLoginGoogle()
  {
    this.afAuth.auth.signInWithPopup(new auth.GoogleAuthProvider());
    this.router.navigate(['/welcome']);
  }

  onLoginFacebook()
  {
    this.afAuth.auth.signInWithPopup(new auth.FacebookAuthProvider());
    this.router.navigate(['/welcome']);
  }

  constructor(public authService:AuthService, private router:Router, private afAuth:AngularFireAuth) { }

  ngOnInit() {
  }

}
