import { Injectable } from '@angular/core';
import { AuthService } from './auth.service';
import { AngularFireDatabase } from '@angular/fire/database';

@Injectable({
  providedIn: 'root'
})
export class GroupsService {

  name = '';
  age = '';
  type = '';
  group = '';
  gender = '';
  checked = false;

  join(name:string, age:string, type:string, group:string, gender:string, checked:boolean)
  {
    this.authService.user.subscribe(user => {
      this.db.list('/users/'+user.uid+'/קבוצות').push({'group':group,'name':name,'age':age,'type':type,'gender':gender,'checked':checked});
    })
  }

  constructor(public authService: AuthService,
              private db: AngularFireDatabase)  { }
}
