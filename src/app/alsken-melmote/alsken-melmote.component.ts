import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'alsken-melmote',
  templateUrl: './alsken-melmote.component.html',
  styleUrls: ['./alsken-melmote.component.css']
})
export class AlskenMelmoteComponent implements OnInit {

  welcome(){
    this.router.navigate(['/dogs']);
  }
  constructor(private router:Router) { }

  ngOnInit() {
  }

}
