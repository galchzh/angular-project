import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AlskenMelmoteComponent } from './alsken-melmote.component';

describe('AlskenMelmoteComponent', () => {
  let component: AlskenMelmoteComponent;
  let fixture: ComponentFixture<AlskenMelmoteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AlskenMelmoteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AlskenMelmoteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
