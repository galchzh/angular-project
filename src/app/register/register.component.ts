import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';
import { Router } from '@angular/router';
import { AngularFireAuth } from '@angular/fire/auth';
import { auth } from 'firebase/app';

@Component({
  selector: 'register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  email: string;
  password: string;
  name: string;
  error = '';
  output = '';
  required = '';
  require = true;
  flag = false;
  specialChar = ['!','@','#','$','%','^','&','*','(',')','_','-','+','='];
  valid = false;
  validation = "";
  
 

  register()
  {
    this.flag = false;
    this.valid = false;
    this.require = true;

    if (this.name == null || this.password == null || this.email == null) 
    {
      this.required = "שדה זה הוא שדה חובה";
      this.require = false;
    }

    if(this.require)
    {
      for (let char of this.specialChar)
      {
        if (this.password.includes(char))
        {
          this.valid = true;
        }
      }
    }
    if (this.valid && this.require)
    {
      this.authService.register(this.email,this.password)
      .then(value => { 
        this.authService.updateProfile(value.user, this.name);
        this.authService.addUser(value.user,this.name, this.email);
      }).then(value => {
        this.router.navigate(['/welcome']);
      }).catch(err => {
        this.flag = true;
        this.error = err.code;
        this.output = err.message;
      })
    }
    else
    {
      this.validation = "הסיסמה חייבת להכיל תווים מיוחדים";
    }
  }

  constructor(public authService:AuthService, private router:Router, public afAuth: AngularFireAuth) { }

  ngOnInit() {
   
  }

  onLoginGoogle()
  {
    this.afAuth.auth.signInWithPopup(new auth.GoogleAuthProvider());
    this.router.navigate(['/welcome']);
  }

  onLoginFacebook()
  {
    this.afAuth.auth.signInWithPopup(new auth.FacebookAuthProvider());
    this.router.navigate(['/welcome']);
  }


}

