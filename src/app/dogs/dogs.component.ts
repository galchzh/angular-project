import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'dogs',
  templateUrl: './dogs.component.html',
  styleUrls: ['./dogs.component.css']
})
export class DogsComponent implements OnInit {

 
  alaskanMalamute(){
    this.router.navigate(['/alskenMelmote']);
  }
  
  alskenKlee(){
    this.router.navigate(['/alskenKlee']);
  }
  amstaff(){
    this.router.navigate(['/amstaff']);
  }
  americanBulldog(){
    this.router.navigate(['/americanBulldog']);
  }
  americanPit(){
    this.router.navigate(['/americanPit']);
  }
  americanEskimos(){
    this.router.navigate(['/americanEskimos']);
  }
  afghan(){
    this.router.navigate(['/afghan']);
  }
  affenpinschers(){
    this.router.navigate(['/affenpinschers']);
  }
  japanisAkita(){
    this.router.navigate(['/japanisAkita']);
  }
   
  constructor(private router:Router) { }


  ngOnInit() {
  }

}
