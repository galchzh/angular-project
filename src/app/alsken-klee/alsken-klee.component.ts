import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'alsken-klee',
  templateUrl: './alsken-klee.component.html',
  styleUrls: ['./alsken-klee.component.css']
})
export class AlskenKleeComponent implements OnInit {

  welcome(){
    this.router.navigate(['/dogs']);
  }
  constructor(private router:Router) { }
  ngOnInit() {
  }

}
