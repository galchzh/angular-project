import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AmericanEskimosComponent } from './american-eskimos.component';

describe('AmericanEskimosComponent', () => {
  let component: AmericanEskimosComponent;
  let fixture: ComponentFixture<AmericanEskimosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AmericanEskimosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AmericanEskimosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
