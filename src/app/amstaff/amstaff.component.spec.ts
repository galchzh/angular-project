import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AmstaffComponent } from './amstaff.component';

describe('AmstaffComponent', () => {
  let component: AmstaffComponent;
  let fixture: ComponentFixture<AmstaffComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AmstaffComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AmstaffComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
