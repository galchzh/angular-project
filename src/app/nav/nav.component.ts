import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../auth.service';

@Component({
  selector: 'nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.css']
})
export class NavComponent implements OnInit {
  toVideo(){
    this.router.navigate(['/categories']);
  }
  toGroup(){
    this.router.navigate(['/groupRegister']);
  }
  toMain(){
    this.router.navigate(['/main']);
  }
  toChat(){
    this.router.navigate(['/chat']);
  }
  toRegister(){
    this.router.navigate(['/register']);
  }
  toWelcome(){
    this.router.navigate(['/welcome']);
  }
  toDogs()
  {
    this.router.navigate(['/dogs']);
  }
  toQuiz(){
    this.router.navigate(['/quiz']);
  }
  toLogin(){
    this.router.navigate(['/login']);
  }
  logout(){
    this.authService.logout().
      then(value => {
        this.router.navigate(['/main'])
          }).catch(err=>{console.log(err)})
  }
  constructor(private router:Router,
              public authService:AuthService
              ) { }


  ngOnInit() {
  }

}
