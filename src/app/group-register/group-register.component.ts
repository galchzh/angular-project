import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AngularFireDatabase } from '@angular/fire/database';
import { GroupsService } from '../groups.service';

@Component({
  selector: 'group-register',
  templateUrl: './group-register.component.html',
  styleUrls: ['./group-register.component.css']
})
export class GroupRegisterComponent implements OnInit {

  dogs = [];
  groups = [];
  name = '';
  age = '';
  type = '';
  group = '';
  gender = '';
  checked = false;

  name_output = '';
  age_output = '';
  type_output = '';
  group_output = '';
  gender_output = '';

  groupRegister()
  {
    if(this.checkRequires())
    {
      this.groupService.join(this.name,this.age,this.type,this.group,this.gender,this.checked);
      this.name = '';
      this.age = '';
      this.type = '';
      this.group = '';
      this.gender = '';
      this.checked = false;
      this.router.navigate(['/success']);
    }
  }

  checkRequires()
  {
    console.log(this.gender_output);
    this.name_output = '';
    this.age_output = '';
    this.type_output = '';
    this.group_output = '';
    this.gender_output = '';
    if(this.name =='')
    {
      this.name_output = "שם הכלב הוא שדה חובה";
      return false;
    }
    if(this.age =='')
    {
      this.age_output = "גיל הכלב הוא שדה חובה";
      return false;
    }
    if(this.type =='')
    {
      this.type_output = "סוג הכלב הוא שדה חובה";
      return false;
    }
    if(this.gender =='')
    {
      this.gender_output = "מין הכלב הוא שדה חובה";
      return false;
    }
    return true;
  }

  constructor( private db:AngularFireDatabase, 
                private groupService:GroupsService,
               private router:Router) { }

  ngOnInit() {

    this.db.list('/כלבים').snapshotChanges().subscribe(
      dogs =>
      {
        this.dogs = [];
        dogs.forEach(
          dog =>
          {
            let m = dog.payload.toJSON();
            this.dogs.push(m);
          }
        )
        console.log(this.dogs);
      }
    )


    this.db.list('/קבוצות').snapshotChanges().subscribe(
      groups =>
      {
        this.groups = [];
       groups.forEach(
          group =>
          {
            let m = group.payload.toJSON();
            this.groups.push(m);
          }
        )
        console.log(this.groups);
      }
    )

  }

}

