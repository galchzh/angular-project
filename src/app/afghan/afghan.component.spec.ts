import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AfghanComponent } from './afghan.component';

describe('AfghanComponent', () => {
  let component: AfghanComponent;
  let fixture: ComponentFixture<AfghanComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AfghanComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AfghanComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
