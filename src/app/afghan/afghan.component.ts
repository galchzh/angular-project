import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'afghan',
  templateUrl: './afghan.component.html',
  styleUrls: ['./afghan.component.css']
})
export class AfghanComponent implements OnInit {

  welcome(){
    this.router.navigate(['/dogs']);
  }
  constructor(private router:Router) { }
  ngOnInit() {
  }

}
