import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-categories',
  templateUrl: './categories.component.html',
  styleUrls: ['./categories.component.css']
})
export class CategoriesComponent implements OnInit {

  toOne()
  {
    this.router.navigate(["/category1"]);
  }

  toTwo()
  {
    this.router.navigate(["/category2"]);
  }

  toThree()
  {
    this.router.navigate(["/category3"]);
  }

  toFour()
  {
    this.router.navigate(["/category4"]);
  }
  constructor(public router:Router) { }

  ngOnInit() {
  }

}
