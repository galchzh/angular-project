
import { Component } from '@angular/core';
import { Quizmodel } from './quizmodel';
import { Answermodel } from './quizmodel';
import { Router } from '@angular/router';

@Component({
  selector: 'quiz',
  templateUrl: './quiz.component.html',
  styleUrls: ['./quiz.component.css']
})
export class QuizComponent {
  constructor(private router:Router) { }
  title = 'quiz';

myarray: String[] = [];
i: number = 0;
languages: String[] = ["סוגים", "ידע כללי"];
 newstr: String
  
  quizlist: Quizmodel[] = [
    {
      ID: 1, language: "סוגים", question: " איזה כלב מהבאים שייך לכלבי צייד   ?", anslistobj: ["לברדור רטריבר", "בורדר קולי", "צ'או צ'או", "שפיץ פומרני"], answer: "לברדור רטריבר"
    },
    {
      ID: 2, language: "סוגים", question: "מה מוצאו של כלב התחש?", anslistobj: ["בלגיה", "גרמניה", "אנגליה", "צרפת"], answer: "גרמניה"
    },
    {
      ID: 3, language: "סוגים", question: "מה היה שם הכלב של אודיסאוס במיתולוגיה היוונית?", anslistobj: ["דיונסיס", "יוליוס", "ארגון", "ארגוס"], answer: "ארגוס"
    },
    {
      ID: 4, language: "סוגים", question: "מה מוצאו של כלב הצ’או צ’או?", anslistobj: ["יפן", "סין", "קוריאה", "תאילנד"], answer: "סין"
    },
    
    {
      ID: 5, language: "ידע כללי", question: "באיזו ארץ התייחסו פעם לכלבים כאלים?", anslistobj: ["יוון", "מצרים", "אנגליה", "צרפת"], answer: "מצרים"
    } ,
    {
      ID: 6, language: "ידע כללי", question: "מה עושים הכלבים במקום להזיע?", anslistobj: ["מלקקים את עצמם", "מתגלגלים במקומות קרים", "מכשכשים בזנב", "מתנשמים עם הלשון בחוץ"], answer: "מתנשמים עם הלשון בחוץ"
    },
    {
      ID: 7, language: "ידע כללי", question: "כמה אצבעות יש לכלב ברגליים האחוריות?", anslistobj: ["3", "4", "5", "6"], answer: "4"
    },
    {
      ID: 8, language: "ידע כללי", question: "מאיזה בעל חיים התפתח גזע הכלב?", anslistobj: ["שועל", "תן", "זאב", "אף אחת מהתשובות"], answer: "זאב"
    }
  ];

  /******************************************************* */
quizlength: number;
selectedlanguage: Quizmodel[] = [];
question: String;
selectedvalue: String;
option: any[];
selectedOpt = '';
selectedlanques: any[];
gettinglanguage() {
this.selectedlanques =  this.quizlist.filter(d => (d.language == this.selectedvalue));
this.question = this.selectedlanques[0].question;
this.option = this.selectedlanques[0].anslistobj;
this.i = 0;
this.quizlength = this.selectedlanques.length;
  }

  /******************************************************** */
  next() {   
    ++this.i;
    this.question = this.selectedlanques[this.i].question;
    this.option = this.selectedlanques[this.i].anslistobj;
  }
  previous() {
    --this.i;
    this.question = this.selectedlanques[this.i].question;
    this.option = this.selectedlanques[this.i].anslistobj;
  }

/********************************************************* */
  
  answerkey: AnswerKey[] = [];

  check( str: String, answer: String) {
    
      console.log("str: "+str + " answer: " + answer);
      this.answerkey.push(new AnswerKey(str, answer));
    // }
    // else {

    //   this.answerkey.splice(0, 1);
    // }
    console.log(this.answerkey);
    this.recursivecheck();
  }
  ///////////////////////////////////

  marks: number = 0;
  generatemark() {
    for (var i = 0; i < this.answerkey.length; i++) {
      console.log("answerkey: "+this.answerkey[i].choosen+"answer: "+this.quizlist[i].answer)
      if(this.selectedvalue =="ידע כללי")
        {
          if (this.answerkey[i].choosen == this.quizlist[i+4].answer) 
          {
          this.marks++;
          console.log("marks: "+this.marks);
          }
        }
        else {
          if (this.answerkey[i].choosen == this.quizlist[i].answer) 
          {
            this.marks++;
            console.log("marks: "+this.marks);
          }
        }
    }
    // console.log("lang:"+this.selectedvalue);
    this.router.navigate(['/score'], { queryParams: { score: this.marks, type:this.selectedvalue } });                                                                                    
    
  } 
    
    

  recursivecheck() {
    var result1 = this.quizlist;
    var result2 = this.answerkey;

    var props = ['id', 'answer'];

    var result = result1.filter(function (o1) {
      // filter out (!) items in result2
      return result2.some(function (o2) {
        return o1.answer === o2.answer;
        // assumes unique id
      });

    }).map(function (o) {

      // use reduce to make objects with only the required properties
      // and map to apply this to the filtered array as a whole
      return props.reduce(function (newo, ans) {
        newo[ans] = o[ans];
        return newo;
      }, {});
    });
    console.log("result:" + JSON.stringify(result));
  }


}

export class AnswerKey {
  choosen: String;
  answer: String;
  constructor(choosen: String, answer: String) {
    this.choosen = choosen;
    this.answer = answer;
  }



}